#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

__author__ = "Dylan Hamel"
__version__ = "1.0"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"

######################################################
#
# Default value used for exit()
#


EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#

try:
    import os
    from os import listdir
    from os.path import isfile, join
except ImportError as importError:
    print("Error import [generate_inventories.py - os]")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from os import sys
except ImportError as importError:
    print("Error import [generate_inventories.py - sys]")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import getopt
except ImportError as importError:
    print("Error import [generate_inventories.py - getopt]")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import yaml
except ImportError as importError:
    print("Error import [generate_inventories.py - yaml]")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import emoji
except ImportError as importError:
    print("Error import [generate_inventories.py - emoji]")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import time
except ImportError as importError:
    print("Error import [generate_inventories.py - time]")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# CONSTANTES
#
HEADER = "###############################################################################\n" +  \
    "################ DO NOT MODIFY THIS IVENTORY FILE MANUALLY !!! ################\n" + \
    "##### https://gitlab.com/DylanHamel/invtentory-file-generator/tree/master #####\n" + \
    "###############################################################################\n"

######################################################
#
# MAIN Functions
#

def main (args):
    """
    This script will create your inventory file
    Please read the README for know the syntax.
    Arguments will be conserv

    :param args:
        Arguments are :
        - [-p] => path to host_vars directory
        - [-i] => path to the inventory file
        - [-g] => mapping group name in your hostname.yml (Ex: --- \n device_group: cisco-router / = -n device_group)

    :return:
    """
    # This is the path to "host_vars" directory. This directory must contains all "hostname.yml" files
    # path = "/etc/ansible/host_vars"

    # This is the path to inventoryFile.
    # inventoryPath = "/etc/ansible/hosts"
    # In your yaml file you have a mapping that describe your device group
    # Example
    # ---
    # device_group: cisco-router
    # mapping = "device_group"

    # Retrieve variables with parameters

    try:
        path, inventoryPath, mapping = retrieveParameters(args)

    except getopt.GetoptError as err:
        print(str(err))
        sys.exit(2)
    except ValueError as err:
        print("Warnings parameters are missing or are not correct")
        print("use -h for help")
        sys.exit(2)

    files = getAllFilesInDirectory(path)

    for key, value in files.items():
        generateHosts(key, value, mapping)

######################################################
#
# Functions
#

def getAllFilesInDirectory(path: str()) -> list():
    """
    This function will return all files contains in a directory and them subdirectory
    
    Args:
            param1 (str): Path to folder. This functions will go to sub-directory

        Returns:
            list: All files in directory and subdirectory
    """

    files = dict()
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path):
        if "host" in str(r):
            files[r+"/"] = str(r[:-9]+"hosts")
    
    return files


def generateHosts(path, inventoryPath, mapping):

    print("Path           = ", path)
    print("Inventory Path = ", inventoryPath)
    print("Mapping        = ", mapping)

    # Execute a LS command in the directory and keep it in a array.
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]

    # For each yaml file.
    # => grep for find the mapping that describe the device type
    mappingHostnameDeviceGroup = dict()
    list_group = list()
    all_device_group = set()
    device_group = ""

    for file in onlyfiles:
        with open(path+file) as stream:
            try:
                f = dict(yaml.load(stream))
                hostname = file[:-4]
                groups = f['device_group']

                if type(groups) == list:
                    for dg in f['device_group']:
                        list_group.append(dg)
                        all_device_group.add(dg)
                    mappingHostnameDeviceGroup[hostname] = list_group
                else:
                    all_device_group.add(groups)
                    mappingHostnameDeviceGroup[hostname] = groups
            except yaml.YAMLError as exc:
                    print("[generate_inventories - main ] Error with file _>", f)

    # First we create the [device_group] if not exist
    fileBody = ""
    ansibleVariablesPart = ""

    if all_device_group != "":
        try:
            inventoryfile = open(inventoryPath, 'r')
            fileBody = inventoryfile.read()
            inventoryfile.close()
            indexAnsibleVariables = fileBody.find("#><!_?")
            ansibleVariablesPart = fileBody[indexAnsibleVariables:]
            fileBody = fileBody[:indexAnsibleVariables]

        except Exception as error:
            print(inventoryPath)
            print("Inventory file not found")

    print(all_device_group)
    for device_gr in all_device_group:
        if (fileBody.find(("["+device_gr+"]"))) == -1 :
            fileBody = ("\n["+device_gr+"]\n")+fileBody

    for key,value in mappingHostnameDeviceGroup.items():
        if type(value) is list:
            for group in value:
                if (fileBody.find((key + "\t\t # " + group)) == -1):
                    stop = 0
                    while (fileBody[stop:].find(key) != -1):
                        indexHostname = fileBody[stop:].find(key)
                        stop = stop + indexHostname
                        # We find the hostname in inventory file
                        indexLength = fileBody[stop+indexHostname:].find("\n")
                        subString = fileBody[stop+indexHostname:indexHostname+indexLength]
                        i = True
                        for model in value:
                            if subString[subString.find("# ")+2:] in model:
                                i = False
                        # if subString[subString.find("# ")+2:-1] not in value:
                        if i is True:
                            fileBody = fileBody[:stop+indexHostname] + \
                                fileBody[stop+indexHostname+indexLength+1:]
                        else:
                            stop = stop + indexLength+1
                    
                    fileBody = addInInventoryFile(fileBody, key, group)

        elif (fileBody.find((key + "\t\t # " + value)) == -1) :
            # Check if the key already exists in the file
            # We search in the file if the host name is already present    
            while (fileBody.find(key) != -1) :
                indexHostname = fileBody.find(key)
                # We find the hostname in inventory file
                indexLength  = fileBody[indexHostname:].find("\n")
                fileBody = fileBody[:indexHostname] + fileBody[indexHostname+indexLength+1:]
        
            fileBody = addInInventoryFile(fileBody, key, value)    
    
    if "gitlab.com/DylanHamel" not in fileBody:
        fileBody = HEADER + fileBody + ansibleVariablesPart

    inventoryfile = open(inventoryPath, 'w')
    inventoryfile.write(fileBody)
    inventoryfile.close()

    print("========================================================================")
    print(fileBody)
    print("========================================================================")
    print("_> Program finished :)", emoji.emojize(
        ':thumbs_up: :thumbs_up: :thumbs_up:'))

# --------------------------------------------------------------------------------------------------
#
#
#
def addInInventoryFile(fileBody, key, value):
    group = ("[" + value + "]")
    index = fileBody.find(value)

    return fileBody[:index+group.__len__()] + key + "\t\t # " + \
    value + "\n" + fileBody[index+group.__len__():]

# --------------------------------------------------------------------------------------------------
#
#
#
def retrieveParameters (args):
    opts, args = getopt.getopt(sys.argv[1:], 'hp:i:g:', ['--help'])
    if sys.argv.__len__() != 7 :
        raise Exception

    for o, a in opts:
        if o == "-p":
            path = a
        elif o == "-i":
            inventoryPath = a
        elif o == "-g":
            mapping = a
        elif o == "-h" or o == "--help":
            print("Please use the following syntax: ")
            print("python3.7 generate_inventories.py -p /etc/ansible/host_vars -i /etc/ansible/hosts -g device_gr")
            print("[-p] = path to host_vars directory")
            print("[-i] = path to inventory file")
            print("[-g] = name of your group in your yaml file (yaml files are in [-p /path]")
            print("--> Check the README for more informations")

    return path, inventoryPath, mapping

# --------------------------------------------------------------------------------------------------
#
#   MAIN
#
if __name__ == "__main__":
   main(sys.argv[1:])

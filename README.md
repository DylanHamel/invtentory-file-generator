# Ansible - Generate your inventory file

###### Dylan Hamel - <dylan.hamel@protonmail.com>

> Information : 
>
> * Improving code is in progress ...



## Requisites

```bash
pip install PyYAML
pip install json
pip install emoji
pip install subprocess
pip install getopt
```

You can create un venv

```shell
python3 -m venv /path/to/repo/ansible-cumulus-cisco

/path/to/repo/invtentory-file-generator /bin/pip  install --upgrade pip
/path/to/repo/invtentory-file-generator /bin/pip install -r requirements.txt
```

### 

### Generate all hosts files

This script will dynamically create your inventories file for Ansible.
With Ansible you can structured your inventory with subdirectory as follow :

---

* inventories/
  * ch/
    * gva/
      * host_vars/
      * group_vars/
      * ```hosts```
    * zrh/
      - host_vars/
      - group_vars/
      - ```hosts```
  * fr/
    * par/
      - host_vars/
      - group_vars/
      - ```hosts```
  * de/
  * it/

---

In this case you can run this script to generate all your ```hosts``` files. The script will go through the inventories directory and search all ```host_vars/``` directory. When a ```host_vars``` directory is found it will search in all yaml file.

```Shell
./generate_inventories.py -d ./inventories -g device_group
# [-d] = path to the inventories directory
# The folder can have a different name
./generate_inventories.py -d ./best_folder -g device_group
```

### Generate 1 hosts file

You can also run this script to generate a ```host``` file.

You can create your own tree in an other directory

---

| ```/etc```

——|—— ```/ansible```

—————|—— ```hosts```

—————|——```/host_vars``` 		

————————|——```inventory_hostname.yml``` 		

————————|——```inventory_hostname.yml``` 		

————————|——```inventory_hostname.yml``` 		

—————|——```/group_vars```

————————|——```cisco_sw.yml``` 		

————————|——```cisco_ios.yml``` 	

---

The script need 3 arguments :

- ```[-p]``` => path to host_vars directory
- ```[-i]``` => path to the inventory file
- ```[-g]``` => mapping group name in your hostname.yml

```shell
./generate_inventories.py -p ./inventories -i /etc/ansible/hosts -g device_group
```

#### ```[-g] = device_group example```

```yaml
---
device_group: cisco-sw
hostname: dh-01-sw-001
domaine_name: dh.local
device_id: sw-01-001
username: admin
password: sw-123123-001
ssh_port: 22
snmp_location: switch-office
```

You can have multiple groups for a device.

```yaml
---
device_group:
  - forti-rt
  - forti-device
  - forti-fw
hostname: dh-01-sw-001
domaine_name: dh.local
device_id: nx-01-001
username: admin
password: sw-123123-001
ssh_port: 22
snmp_location: switch-bureau
```

=> ```device_group: cisco-sw```

You can rename this group. If you use a key as ```group``` run the script

```bash
./generate_inventories.py -p /etc/ansible/host_vars -i /etc/ansible/hosts -g group
```

=> ```-g group```

### Help

You can ask help

```shell
./generate_inventories.py --help
```



### Variables

This script will not remove your Ansible variables. Add following lines above them

```shell
#><!_?#####################################################
# Host Variables
#><!_?#####################################################
```

Example 

```bash
#><!_?#####################################################
# Host Variables
#><!_?#####################################################
[cisco-ios:vars]
host_key_checking=false
ansible_user=admin
ansible_ssh_pass=cisco
ansible_port=22
ansible_ssh_common_args="-o HostKeyAlgorithms=ssh-rsa -o KexAlgorithms=diffie-hellman-group1-sha1 -o Ciphers=aes256-cbc,3des-cbc -o MACs=hmac-md5,hmac-sha2-512 -o StrictHostKeyChecking=no"
```



### Result and Example

```bash
./generate_inventories.py -p ./host_vars -i ./hosts -g device_group
```

```bash
###############################################################################
################ DO NOT MODIFY THIS IVENTORY FILE MANUALLY !!! ################
##### https://gitlab.com/DylanHamel/invtentory-file-generator/tree/master #####
###############################################################################

[cisco-sw]
dh-01-sw-003		 # cisco-sw
dh-01-sw-001		 # cisco-sw

[forti-fw]
aa-01-sw-002		 # forti-fw

[arista]
aa-01-sw-001		 # arista

[cisco-rt]
dh-01-rt-002		 # cisco-rt
dh-01-rt-001		 # cisco-rt

[cisco-nx]
dh-01-nx-002		 # cisco-nx
dh-01-nx-001		 # cisco-nx

[forti-device]
aa-01-sw-002		 # forti-device

[forti-rt]
aa-01-sw-002		 # forti-rt
dh-01-sw-002		 # forti-rt
#><!_?#####################################################
# Host Variables
#><!_?#####################################################
[cisco-ios:vars]
host_key_checking=false
ansible_user=admin
ansible_ssh_pass=cisco
ansible_port=22
ansible_ssh_common_args="-o HostKeyAlgorithms=ssh-rsa -o KexAlgorithms=diffie-hellman-group1-sha1 -o Ciphers=aes256-cbc,3des-cbc -o MACs=hmac-md5,hmac-sha2-512 -o StrictHostKeyChecking=no"
```



Please **DON'T REMOVE THE** ```\t\t # group```.



if you chnage the ```device_group``` for a host:

```yaml
---
device_group: core-device
hostname: dh-01-sw-002
domaine_name: dh.local
device_id: sw-01-002
username: admin
password: sw-123123-002
ssh_port: 22
snmp_location: switch-bureau
```

```bash
./generate_inventories.py -p /etc/ansible/host_vars -i /etc/ansible/hosts -g device_group
```

The result

```bash
###############################################################################
################ DO NOT MODIFY THIS IVENTORY FILE MANUALLY !!! ################
##### https://gitlab.com/DylanHamel/invtentory-file-generator/tree/master #####
###############################################################################

[cisco-sw]
dh-01-sw-003		 # cisco-sw
dh-01-sw-001		 # cisco-sw

[forti-fw]
aa-01-sw-002		 # forti-fw

[arista]
aa-01-sw-001		 # arista

[cisco-rt]
dh-01-rt-002		 # cisco-rt
dh-01-rt-001		 # cisco-rt

[cisco-nx]
dh-01-nx-002		 # cisco-nx
dh-01-nx-001		 # cisco-nx

[forti-device]
aa-01-sw-002		 # forti-device

[forti-rt]
aa-01-sw-002		 # forti-rt

[core-device]
dh-01-sw-002		 # core-device
#><!_?#####################################################
# Host Variables
#><!_?#####################################################
[cisco-ios:vars]
host_key_checking=false
ansible_user=admin
ansible_ssh_pass=cisco
ansible_port=22
ansible_ssh_common_args="-o HostKeyAlgorithms=ssh-rsa -o KexAlgorithms=diffie-hellman-group1-sha1 -o Ciphers=aes256-cbc,3des-cbc -o MACs=hmac-md5,hmac-sha2-512 -o StrictHostKeyChecking=no"
```

=> **Old group is not remove**

